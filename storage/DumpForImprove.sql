﻿CREATE DATABASE  IF NOT EXISTS `improvetest` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `improvetest`;
-- MySQL dump 10.13  Distrib 5.6.17, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: improvetest
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (1,'Books'),(2,'Movie'),(3,'Music'),(4,'Soft'),(5,'Series'),(6,'Games');
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cat_id` (`cat_id`),
  CONSTRAINT `fk_cat_id` FOREIGN KEY (`cat_id`) REFERENCES `catalog` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,2,'The Shawshank Redemption',150.00),(2,2,'The Godfather',100.00),(3,2,'The Godfather: Part II',100.00),(4,6,'Skyrim',300.00),(5,2,'The Dark Knight',349.00),(6,2,'Pulp Fiction',75.00),(7,2,'12 Angry Men',109.00),(8,2,'Schindler\'s List',150.00),(9,2,'Il buono, il brutto, il cattivo',99.99),(10,2,'The Lord of the Rings: The Return of the King',200.50),(11,2,'Fight Club',137.00),(12,1,'Dune',377.00),(13,1,'Ender’s Game',256.00),(14,1,'Starship Troopers',157.80),(15,1,'Foundation',560.00),(16,1,'The Stars My Destination',344.00),(17,1,'2001: A Space Odyssey ',260.00),(18,1,'Hyperion Cantos',400.00),(19,1,'Neuromancer',370.00),(20,3,'Dark Side of the Moon',89.00),(21,3,'Sgt. Pepper\'s Lonely Hearts Club Band',58.00),(22,3,'Led Zeppelin IV',98.00),(23,3,'Thriller',55.00),(24,3,'Abbey Road',66.00),(25,3,'Nevermind',78.00),(26,3,'A Night at the Opera',55.00),(27,3,'Pet Sounds',80.00),(28,4,'Lingvo 10',499.00),(29,4,'Adobe Dreamweaver CS6 12',4949.00),(30,4,'Adobe Illustrator CS6 16',10999.00),(31,4,'Microsoft Office 2013 Home',4490.00),(32,4,'Microsoft Windows 8.1',4390.00),(33,4,'Microsoft Windows 7 Home Basic',6390.00),(34,4,'Kaspersky Antivirus 2015',1199.00),(35,5,'True Detective',399.00),(36,5,'Game of Thrones',590.00),(37,5,'The Walking Dead',460.00),(38,5,'Suits',220.00),(39,5,'Breaking Bad',300.00),(40,5,'Friends',199.00),(41,5,'Sherlock',249.00),(42,5,'House of Cards',289.00),(43,6,'Sid Meier\'s Civilization V',499.00),(44,6,'Grand Theft Auto V',1999.00),(45,6,'Fallout: New Vegas',219.00),(46,6,'Left 4 Dead 2',499.00),(47,6,'DayZ',1399.00),(48,6,'The Witcher 3: Wild Hunt',1199.00),(49,6,'Portal 2',399.00);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-11 10:33:59
