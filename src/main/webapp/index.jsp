<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
		"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Test Web Application</title>
<style type="text/css">
	<%@include file="/css/bootstrap.css" %>
</style>
</head>
<body>
	<c:set var="queryUrl" value="${pageContext.request.queryString}" />
	<c:set var="servletUurl"
		value="${requestScope['javax.servlet.forward.request_uri']}" />
	<c:if test="${not empty queryUrl}">
		<c:set var="queryUrl" value="&${pageContext.request.queryString}" />
	</c:if>
	<fmt:parseNumber var="page" type="number" value="${currentPage}" />
	<fmt:parseNumber var="pages" type="number" value="${pagesAll}" />
	
	<form action="" method="get">
		<div class="container-fluid">


			<h2 class="text-center bold">Price List</h2>
			</br>
			<div class="container">
				<div class="form-inline">
					<div class="form-group">
						<label for="category">Category:</label> <input type="text"
							name="category" placeholder="Category" class="form-control"
							value="${category}">
					</div>
					<div class="form-group">
						<label for="name">Name:</label> <input type="text" name="name"
							placeholder="Name" class="form-control" value="${name}">
					</div>
					<div class="form-group">
						<label for="priceFrom">Price From:</label> <input type="text"
							name="price-from" placeholder="Price From" class="form-control"
							value="${(pricefrom != '0') ? pricefrom : ''}">
					</div>
					<div class="form-group">
						<label for="priceTo">Price To:</label> <input type="text"
							name="price-to" placeholder="Price To" class="form-control"
							value="${(priceto != '0') ? priceto : ''}">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">Search</button>
					</div>

				</div>
				</br>

				<div class="form-inline center-block">
					<div class="form-group pull-right">
						<a href="${servletUrl}?" class="btn btn-default btn-block active">Clear</a>
					</div>
				</div>

			</div>
		</div>

		</br>

		<div class="container">
			<table class="table table-bordered table-striped">
				<tr>
					<th>Category</th>
					<th>Name</th>
					<th>Price</th>
				</tr>

				<c:forEach var="product" items="${productList}">
					<tr>
						<td>${product.catalog.name}</td>
						<td>${product.name}</td>
						<td>${product.price}</td>
					</tr>
				</c:forEach>
			</table>

			<nav>
			<ul class="pager">
				<li class="${(page == 1) ? 'disabled' : 'active'}">
					<a href="${servletUrl}?page=${page - 1}${queryUrl}">Previous</a></li>
				<li><a class="btn btn-default disabled">Page ${currentPage} of
						${pagesAll}</a></li>
				<li class="${(page == pagesAll) ? 'disabled' : 'active'}">
					<a href="${servletUrl}?page=${page + 1}${queryUrl}">Next</a></li>
			</ul>
			</nav>

		</div>
	</form>
</body>
</html>