package com.improve.apptest;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.improve.apptest.model.Product;

public class WebAppServlet extends HttpServlet {
    
    private static final long serialVersionUID = 102831973239L;
    
    public WebAppServlet() {
    	super();
    }
 
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	WebAppPersist persist = new WebAppPersist();
    	
    	String category = request.getParameter("category");
    	String name = request.getParameter("name");

    	BigDecimal priceFrom = parseBigDecimal(request.getParameter("price-from"));
    	BigDecimal priceTo = parseBigDecimal(request.getParameter("price-to"));

    	int limit = 10;
    	int currentPage = 1;
    	
    	if (request.getParameter("page") != null) 
    		currentPage = Integer.parseInt(request.getParameter("page")); 
    	int pagesAll = persist.countProducts(category, name, priceFrom, priceTo) / limit + 1;
    	
    	if (currentPage > pagesAll)
    		currentPage = pagesAll;
    	else if (currentPage < 1)
    		currentPage = 1;
    	
    	List<Product> list = persist.productList(category, name, priceFrom, priceTo, limit, (currentPage - 1) * limit);
    	request.setAttribute("category", category);
    	request.setAttribute("name", name);
    	request.setAttribute("pricefrom", priceFrom);
    	request.setAttribute("priceto", priceTo);
    	
		request.setAttribute("productList", list);
		request.setAttribute("pagesAll", pagesAll);
		request.setAttribute("currentPage", currentPage);
		RequestDispatcher view = request.getRequestDispatcher("index.jsp");
		view.forward(request, response);
    }
    
    public BigDecimal parseBigDecimal(String str) {
    	Locale in_ID = new Locale("in","ID");

        DecimalFormat nf = (DecimalFormat)NumberFormat.getInstance(in_ID);
        nf.setParseBigDecimal(true);
        
        if (str != null) return (BigDecimal)nf.parse(str, new ParsePosition(0));
        else return new BigDecimal(0);
    }
}