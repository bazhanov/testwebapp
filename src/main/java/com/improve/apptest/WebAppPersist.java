package com.improve.apptest;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.improve.apptest.model.Product;

public class WebAppPersist {
	
	public static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	@SuppressWarnings("unchecked")
	public List<Product> productList(String category, 
											String name, 
											BigDecimal priceFrom, 
											BigDecimal priceTo,
											int limit,
											int offset) {
		Session session = getSessionFactory().openSession();
		
		try {
			if (name == null) name = "";
			if (category == null) category = "";
			String hql = "select pr from Product pr where pr.name like :name and "
					+ "pr.catalog.name like :category ";
			if (priceFrom != null) 
				hql += "and pr.price >= :priceFrom ";
			if (priceTo != null)
				if (priceTo.longValue() > 0)
					hql += "and pr.price <= :priceTo";
			
			Query query = session.createQuery(hql)
					.setParameter("name", name + "%")
					.setParameter("category", category + "%");
			if (priceFrom != null)
				query.setParameter("priceFrom", priceFrom);
			if (priceTo != null)
				if (priceTo.longValue() > 0)
					query.setParameter("priceTo", priceTo);
			
			return query.setFirstResult(offset).setMaxResults(limit).list();
		} catch (NoResultException e) {
			return null;
		}
	}

	public int countProducts(String category, 
											String name, 
											BigDecimal priceFrom, 
											BigDecimal priceTo) {
		Session session = getSessionFactory().openSession();
		
		try {
			if (name == null) name = "";
			if (category == null) category = "";
			String hql = "select count(pr) from Product pr where pr.name like :name and "
					+ "pr.catalog.name like :category ";
			if (priceFrom != null) 
				hql += "and pr.price >= :priceFrom ";
			if (priceTo != null)
				if (priceTo.longValue() > 0)
					hql += "and pr.price <= :priceTo";
			
			Query query = session.createQuery(hql)
					.setParameter("name", name + "%")
					.setParameter("category", category + "%");
			if (priceFrom != null)
				query.setParameter("priceFrom", priceFrom);
			if (priceTo != null)
				if (priceTo.longValue() > 0)
					query.setParameter("priceTo", priceTo);
			
			return ((Long)query.uniqueResult()).intValue();
		} catch (NoResultException e) {
			return 0;
		}
	}
}
